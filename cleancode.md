# Чеклист и инструкция в Линт-Вегасе

## Pre-Commit

* Установить [pre-commit](https://pre-commit.com/#installation)
* Создать в корне проекта файл `.pre-commit-config.yaml` с таким
  содержимым:

  ```yaml
  repos:
  - repo: https://gitlab.tochka-tech.com/compliance/scala/pre-commit-cs.git
    rev: "текущий хэш или ревизия шаблона в репозитории"
    hooks:
      - id: check-build-files
      - id: sbt-compile-and-check
  ```

  > Текущий хэш можно посмотреть например в гитлабе под кнопкой `Web
  > IDE`.  
  > Ключ `repos:repo` указывает на
  > [репозиторий с хуками](https://gitlab.tochka-tech.com/compliance/scala/pre-commit-cs).

* В проекте запустить команду `pre-commit install`

Текущие хуки делают две вещи:

* проверяют установленные плагины и инструменты
* Запускают эти плагины и компилируют код

## Плагины

Нужно добавить в `plugins.sbt` следующие плагины, а для некоторых еще и
конфигурацию в `build.sbt` или отдельный конфиг в корень проекта:

* [Wartremover](https://github.com/wartremover/wartremover) +
  [Wartremover-Contrib](https://github.com/wartremover/wartremover-contrib) – линтер

  ```scala
  wartremoverWarnings in (Compile, compile) ++= Seq(
      Wart.ExplicitImplicitTypes,
      Wart.FinalVal,
      Wart.LeakingSealed,
      Wart.Option2Iterable,
      Wart.PublicInference,
      Wart.TryPartial,
      ContribWart.ExposedTuples,
      ContribWart.OldTime,
      ContribWart.SealedCaseClass,
      ContribWart.RefinedClasstag,
      ContribWart.UnsafeInheritance, //не обязательный, иначе устанете везде final добавлять
      ContribWart.SomeApply
  )
  ```
* [Tpolecat](https://github.com/DavidGregory084/sbt-tpolecat) – рекомендованные флаги для компилятора
* [Scoverage](https://github.com/scoverage/sbt-scoverage) – покрытие тестами

  ```scala
  coverageMinimum in ThisBuild := 80
  coverageFailOnMinimum in ThisBuild := true
  ```

* [Scalastyle-sbt](http://www.scalastyle.org/sbt.html) – линтер стиля

  ```scala
  scalastyleFailOnError := true
  scalastyleFailOnWarning := false
  scalastyleConfigUrl := Some(new URL("https://gitlab.tochka-tech.com/compliance/scala/pre-commit-cs/raw/master/scalastyle-config.xml?inline=false"))
  scalastyleConfigUrlCacheFile := "../scalastyle-config.xml"
  ```
* [Scalafmt](https://github.com/scalameta/sbt-scalafmt) – форматирование кода

  > [Конфиг](./.scalafmt.conf) нужно положить в корень проекта

* [Sbt-updates](https://github.com/rtimush/sbt-updates) – обновления зависимостей
* [Scapegoat 👑](https://github.com/sksamuel/sbt-scapegoat) – еще один
  линтер

  ```scala
  scapegoatVersion in ThisBuild := "1.4.1"
  scapegoatConsoleOutput in ThisBuild := false
  scapegoatRunAlways in ThisBuild := false
  ```

  > Может быть так, что в проекте еще очень долго не получится поднять
  > test coverage до 80%, но подключить pre-commit хочется уже сейчас,
  > тогда есть небольшой лайфак: хуки ищут конкретную строчку
  > `coverageMinimum in ThisBuild := 80`, которую можно довести до
  > нужного уровня простым вычитанием — `coverageMinimum in ThisBuild :=
  > 80 - 75`.

## Проверки

Сейчас можно запустить pre-commit на своем проекте:

```
pre-commit run --all-files --verbose
```

так можно проверять, ок ли все с файлами без необходимости что-то
коммитить.

* Для того, чтобы чинить ошибки `Scalastyle` можно пользоваться IntelliJ
  IDEA: скачиваем конфиг `scalastyle-config.xml` (ссылка есть в
  `build.sbt`) и запускаем в идее `Analyze` -> `Inspect Code`, тыкаем на
  `…` в блоке `Inspection Profile` и ставим галку в `Scala` -> `Code
  Style` -> `Scala style inspections`, запускаем. Появится список
  проблем, который удобнее, чем вывод в консоли. К сожалению, идея умеет
  только показывать эти проблемы, но не чинить :(

* `Scapegoat` тоже удобнее запускать из `sbt` командой `scapegoat`. Он
  выдает html вывод, в котором более понятные сообщения о проблемах, но
  нет кликабельных ссылок на файлы, хотя в сбт они есть. Лучше
  использовать оба вида вывода.

# Плагины билда

* [silencer](https://github.com/ghik/silencer) – поможет заглушить
  ложно-положительные варнинги
* [kind-projector](https://github.com/typelevel/kind-projector) -
  позволяет писать вместо `{type F[A] = E[A, Throwable]}#F` – `E[*,
  Throwable]`
* [better-monadic-for](https://github.com/oleg-py/better-monadic-for) –
  плюшки для for-comprehension

