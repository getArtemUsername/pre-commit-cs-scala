REQUIRED_BUILD_PLUGINS = [
    'sbt-wartremover',
    'sbt-wartremover-contrib',
    'sbt-tpolecat',
    'sbt-scoverage',
    'scalastyle-sbt-plugin',
    'sbt-scalafmt',
    'sbt-updates',
    'sbt-scapegoat'
]

REQUIRED_COMPILER_PLUGINS = [
    'better-monadic-for',
    'kind-projector',
    'silencer-lib',
    'silencer-plugin'
]

REQUIRED_SBT_KEYS = [
    'scalastyleFailOnError := true',
    'scalastyleFailOnWarning := false',
    'scalastyleConfigUrl := Some(',
    'scalastyleConfigUrlCacheFile := "../scalastyle-config.xml"',
    'scapegoatVersion in ThisBuild := "1.4.1"',
    'scapegoatRunAlways in ThisBuild := false',
    'coverageMinimum in ThisBuild := 80',
    'coverageFailOnMinimum in ThisBuild := true',
    'wartremoverWarnings in (Compile, compile)',
    'Wart.ExplicitImplicitTypes',
    'Wart.FinalVal',
    'Wart.LeakingSealed',
    'Wart.Option2Iterable',
    'Wart.PublicInference',
    'Wart.TryPartial',
    'ContribWart.ExposedTuples',
    'ContribWart.OldTime',
    'ContribWart.SealedCaseClass',
    'ContribWart.RefinedClasstag',
    'ContribWart.SomeApply',
    'scalacOptions += "-P:silencer:checkUnused"',
    'addCommandAlias',
    'checks', 
    '; clean; compile; test:compile; scalafmt; scalafmtCheck; scalastyle; scapegoat;'
]


def main(argv=None):
    with open('./project/plugins.sbt') as f:
        content = f.read()

        for l in REQUIRED_BUILD_PLUGINS:
            if l not in content:
                print(f'Add build plugin {l} to plugins.sbt')
                return 1

    with open('./build.sbt') as f:
        content = f.read()
        for l in REQUIRED_SBT_KEYS:
            if l not in content:
                print(f'Add {l} line to build.sbt')
                return 1

    print("All good")


if __name__ == '__main__':
    exit(main())
